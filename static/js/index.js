// 日期节点
let num = document.querySelectorAll('.fil')[0];
// 姓名
let name = document.querySelectorAll('.fil')[1];
// 文件
let files = document.querySelector('.clip');
// 图片预览
let show = document.querySelector('.show');
// 提交按钮盒子
let sub = document.querySelector('.sub');
//提交按钮
let subm = document.querySelector('.subm');
//日期
let date = document.querySelector('.fildate');
// 加载层
let load = document.querySelector('.load');
// form表单
let form_info = document.querySelector('.main')
// 文件输入
let fileInput = document.querySelector('#selectFile');
let previewImg = document.querySelector('.show img');
// 添加图片预览
fileInput.addEventListener('change', function () {
    var file = this.files[0];
    var reader = new FileReader();
    // 监听reader对象的的onload事件，当图片加载完成时，把base64编码賦值给预览图片
    reader.addEventListener("load", function () {
        show.style.display = 'block';
        previewImg.src = reader.result;
    }, false);
    // 调用reader.readAsDataURL()方法，把图片转成base64
    reader.readAsDataURL(file);
}, false);

// 点击提交
sub.onclick = function () {
    // 学号不能为空
    if (num.value) {
        console.log(num.value);
        if (num.value.length !== 11) {
            alert('请检查学号是否正确！');
            return;
        }
    } else {
        alert('请输入非空内容')
        return;
    }
    if (!name.value) {
        alert('请输入非空内容')
        return;
    }
    if (!files.value) {
        alert("文件不能为空")
        return;
    }
    let d = String(date.value);
    let inputDate = parseInt(d.split('-')[2]);
    let dat = new Date();
    let day = dat.getDate();
    // 日期不能过大
    if (inputDate > day) {
        alert('日期不符合实际,请重新选择日期!');
        return;
    }
    if (!date.value) {
        alert('请选择日期！');
        return;
    }
    // 设置加载层的显示
    load.style.display = 'block';
    subm.disabled = true;
    // 触发提交
    form_info.submit();
}



